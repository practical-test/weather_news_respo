package com.app.weathernews.base.ui

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity

open class BaseActivity : AppCompatActivity(), View.OnClickListener {
    open fun setupObserver() {
    }

    open fun iniObject() {
    }

    open fun initLocalUI() {
    }

    open fun registerListener() {
    }

    open fun setUIData() {
    }

    open fun getIntentData() {
    }

    override fun onClick(view: View) {
    }

    protected open fun hideSoftKeyBoard(context: Context) {
        val view = currentFocus
        if (view != null) {
            val imm: InputMethodManager =
                (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    } //end of hideSoftKeyBoard()
}