package com.app.weathernews.base.network

enum class Status {
    SUCCESS,
    ERROR,
    LOADING,
    VALIDATION_ERROR,
    NO_NETWORK
}