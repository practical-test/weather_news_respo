package com.app.weathernews.base.network

data class NetworkError(
    val message: String,
    val errors: HashMap<String, List<String>>?
)