package com.app.weathernews.base.repository

import android.util.Log
import com.app.weathernews.base.extension.TAG
import com.app.weathernews.base.network.ApiResult
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

abstract class BaseRepository {

    suspend fun <T> ioCall(
        call: suspend () -> T,
        returnDispatcher: CoroutineDispatcher = Dispatchers.Main
    ): ApiResult<T> {
        val result = withContext(Dispatchers.IO) {
            try {
                val result = call.invoke()
                ApiResult.Success(result)
            } catch (http: HttpException) {
                buildError(http)
            } catch (socket: SocketTimeoutException) {
                ApiResult.Error(CODE_SOCKET_TIMEOUT, "Timeout")
            } catch (host: UnknownHostException) {
                ApiResult.Error(CODE_UNKNOWN_HOST, "No internet connectivity")
            } catch (ex: Exception) {
                Log.e(TAG, "Error with network call", ex)
                ApiResult.Error(UNKNOWN_ERROR, "Unknown error")
            }
        }
        return withContext(returnDispatcher) { result }
    }

    abstract fun buildError(error: Throwable): ApiResult.Error

    companion object {
        const val UNKNOWN_ERROR = -1
        const val CODE_UNKNOWN_HOST = -2
        const val CODE_SOCKET_TIMEOUT = 408
    }
}