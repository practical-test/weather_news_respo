package com.app.weathernews.base.ui

import android.view.View
import androidx.fragment.app.Fragment

open class BaseFragment : Fragment(), View.OnClickListener {
    open fun setupObserver() {
    }

    open fun iniObject() {
    }

    open fun initUiView() {
    }

    open fun initLocalUI() {
    }

    open fun registerListener() {
    }

    open fun setUIData() {
    }

    override fun onClick(view: View) {
    }
}