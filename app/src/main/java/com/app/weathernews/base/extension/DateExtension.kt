package com.app.weathernews.base.extension

import com.app.weathernews.util.DateUtils
import java.text.SimpleDateFormat
import java.util.*

fun Date.changeDateFormat(requireFormat: String = DateUtils.DEFAULT_DATE_FORMAT_OUTPUT): String {
    val sdf = SimpleDateFormat(requireFormat, Locale.getDefault())
    return sdf.format(this)
}