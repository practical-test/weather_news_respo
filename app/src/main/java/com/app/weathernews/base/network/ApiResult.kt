package com.app.weathernews.base.network

sealed class ApiResult<out T> {
    data class Success<out T>(val data: T) : ApiResult<T>()
    data class Error(val code: Int?, val message: String?) : ApiResult<Nothing>()
    data class NoInternet<out T>(val data: T) : ApiResult<T>()
}