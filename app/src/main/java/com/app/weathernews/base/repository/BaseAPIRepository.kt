package com.app.weathernews.base.repository

import android.util.Log
import com.app.weathernews.base.extension.TAG
import com.app.weathernews.base.network.ApiResult
import com.app.weathernews.base.network.NetworkError
import com.google.gson.Gson
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

abstract class BaseAPIRepository : BaseRepository() {
    override fun buildError(error: Throwable): ApiResult.Error {
        Log.w(TAG, "Api error", error)
        var message = "Unknown error"
        var code =
            UNKNOWN_ERROR
        when (error) {
            is HttpException -> {
                error.response()?.let {
                    val errorBody = it.errorBody()?.string() ?: message
                    try {
                        val networkError = Gson().fromJson(errorBody, NetworkError::class.java)
                        networkError.message.let { errorMsg->
                            message = errorMsg
                        }

                        networkError.errors?.let { errors ->
                            val count = errors.size
                            var processed = 0
                            message += " ("
                            errors.forEach { (key, _) ->
                                message += key
                                if (++processed < count) {
                                    message += ", "
                                }
                            }
                            message += ")"
                        }
                    } catch (e: JSONException) {
                        Log.w(TAG, "Failed json exception", e)
                    } catch (e: com.google.gson.JsonSyntaxException) {
                        val jsonObject = JSONObject(errorBody)
                        if (jsonObject.has("error")) {
                            message = jsonObject.optString("error")
                        } else if (jsonObject.has("errors")) {
                            val errors = jsonObject.getJSONArray("errors")
                            val msg = StringBuilder()
                            for (i in 0 until errors.length()) {
                                msg.append(errors[i])
                                if (i <= errors.length()) {
                                    msg.append(",\n")
                                }
                            }
                            message = msg.toString()
                        }

                    } catch (e: Exception) {
                        Log.w(TAG, "Failed parsing error json", e)
                    }
                    code = it.code()
                    if (message == null) {
                        val jsonObject = JSONObject(errorBody)
                        if (jsonObject.has("error")) {
                            message = jsonObject.optString("error")
                        }
                    }
                }
            }
            is SocketTimeoutException -> message = "Connection Timeout"
            is UnknownHostException -> message = "No internet connection"
        }

        return ApiResult.Error(code, message)
    }
}