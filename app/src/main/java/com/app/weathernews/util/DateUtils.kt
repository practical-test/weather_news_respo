package com.app.weathernews.util

object DateUtils {
    const val DEFAULT_DATE_FORMAT_OUTPUT = "dd MMM, yyyy hh:mm aa"
    const val DATE_FORMAT_WITH_SECOND = "dd MMM, yyyy hh:mm:ss aa"
}