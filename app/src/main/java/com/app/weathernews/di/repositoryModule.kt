package com.app.weathernews.di

import com.app.weathernews.data.repository.WeatherLocalRepository
import com.app.weathernews.data.repository.WeatherRepository
import com.app.weathernews.data.repository.WeatherRepositoryImpl
import com.app.weathernews.data.service.WeatherConfigService
import org.koin.dsl.module

var repositoryModule = module {
    factory { WeatherConfigService.create() }
    factory { WeatherLocalRepository(get()) }
    factory {
        WeatherRepositoryImpl(
            service = get(),
            weatherLocalRepository = get(),
            networkHelper = get()
        ) as WeatherRepository
    }
}