package com.app.weathernews.di

import com.app.weathernews.ui.weather.WeatherViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

var viewModelModule = module {
    viewModel {
        WeatherViewModel(
            context = get(),
            weatherRepository = get(),
            weatherLocalRepository = get()
        )
    }
}