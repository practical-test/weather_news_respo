package com.app.weathernews.di

import android.app.Application
import androidx.room.Room
import com.app.weathernews.data.db.WeatherDatabase
import com.app.weathernews.data.db.dio.LocationDataDao
import com.app.weathernews.data.db.dio.TemperatureDao
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

var databaseModule = module {
    fun provideDatabase(application: Application): WeatherDatabase {
        return Room.databaseBuilder(
            application,
            WeatherDatabase::class.java,
            WeatherDatabase.DB_NAME
        )
            .fallbackToDestructiveMigration()
            .build()
    }

    fun providesTemperatureDao(db: WeatherDatabase): TemperatureDao = db.getTemperatureDao()
    fun providesLocationDataDao(db: WeatherDatabase): LocationDataDao = db.getLocationDataDao()

    single { provideDatabase(androidApplication()) }
    single { providesTemperatureDao(get()) }
    single { providesLocationDataDao(get()) }
}


