package com.app.weathernews.di

import android.content.Context
import com.app.weathernews.base.network.NetworkHelper
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val coreNetworkModule = module {
    single(named("default")) { getOkHttpClient() }
    factory(named("timeout")) { (timeout: Long) -> getOkHttpClient(timeout) }
    factory { (baseUrl: String) -> getDefaultRetrofitBuilder(baseUrl) }
    single { provideNetworkHelper(androidContext()) }
}

internal fun provideNetworkHelper(context: Context) = NetworkHelper(context)

internal fun getDefaultRetrofitBuilder(baseUrl: String): Retrofit.Builder {
    return Retrofit.Builder()
        .baseUrl(baseUrl)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
}

internal fun getOkHttpClient(timeout: Long = 10): OkHttpClient {
    return OkHttpClient.Builder().apply {
        writeTimeout(timeout, TimeUnit.SECONDS)
        readTimeout(timeout, TimeUnit.SECONDS)
        connectTimeout(timeout, TimeUnit.SECONDS)
    }.build()
}