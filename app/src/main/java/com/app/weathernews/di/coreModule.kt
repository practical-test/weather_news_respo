package com.app.weathernews.di

import android.content.Context
import com.google.android.gms.location.LocationServices
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val coreModule = module {
    single { provideFusedLocationClient(androidContext()) }
}

internal fun provideFusedLocationClient(context: Context) =
    LocationServices.getFusedLocationProviderClient(context)
