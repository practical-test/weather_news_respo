package com.app.weathernews.ui.weather

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.weathernews.R
import com.app.weathernews.base.extension.*
import com.app.weathernews.base.network.Status
import com.app.weathernews.base.ui.BaseActivity
import com.app.weathernews.data.response.weather.Temperature
import com.app.weathernews.databinding.ActivityWeatherBinding
import com.app.weathernews.ui.weather.adapter.TemperatureAdapter
import com.app.weathernews.util.EndlessRecyclerOnScrollListener
import com.app.weathernews.util.LocationPermissionUtils
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

class WeatherActivity : BaseActivity() {

    private val viewModel: WeatherViewModel by viewModel()
    private lateinit var binding: ActivityWeatherBinding
    private val fusedLocationClient: FusedLocationProviderClient by inject()
    private var temperatureAdapter: TemperatureAdapter? = null
    private var temperatureList = mutableListOf<Temperature>()
    private var isLoadMore = true
    private var page = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_weather)

        binding.viewModel = viewModel
        setupObserver()
        viewModel.getAllTemperatureFromDB()
        setAdapter()
    }

    override fun setupObserver() {
        //observer temperature data from remote repository, if internet is not available than it will be loaded local repository
        viewModel.temperature.observe(this, {
            when (it.status) {
                Status.LOADING -> {
                    binding.progressBar.visible()
                }
                Status.SUCCESS -> {
                    binding.progressBar.gone()
                    it.data?.let { temperature ->
                        setData(temperature)
                    }
                }
                Status.NO_NETWORK -> {
                    binding.progressBar.gone()
                    it.message?.let { msg ->
                        toast(msg)
                    }
                }
                Status.ERROR -> {
                    binding.progressBar.gone()
                    it.message?.let { msg ->
                        toast(msg)
                    }
                }
                else -> {
                    binding.progressBar.gone()
                    toast(resources.getString(R.string.ADMIN_ERROR))
                }
            }
        })

        //live data observer, retrieved from local database
        viewModel.lastLocalLocation.observe(this, { locationData ->
            locationData?.let {
                viewModel.getWeatherAPICall(it.lat, it.lon)
            } ?: run {
                viewModel.getWeatherAPICall(0.0, 0.0)
            }
        })

        //live data observer for all temperature data from local database, page wise
        viewModel.temperatureAll.observe(this, { tempList ->
            if (tempList.isNotEmpty()) {
                temperatureList.addAll(tempList)
                binding.tvRecentSavedTemp.visible()
                temperatureAdapter?.notifyDataSetChanged()
            } else {
                isLoadMore = false
            }
        })
    }

    private fun setData(temperature: Temperature) {
        binding.tvTemperature.text =
            StringBuilder(resources.getString(R.string.temperature))
                .append(" ")
                .append(temperature.temp.toInt())
                .append(" ")
                .append(resources.getString(R.string.celsius))

        val date = Date(temperature.created_time)
        binding.tvDate.text =
            StringBuilder(resources.getString(R.string.date))
                .append(" ")
                .append(date.changeDateFormat())
    }

    private fun setAdapter() {
        binding.rvTemperature.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            temperatureAdapter = TemperatureAdapter(context, temperatureList)
            adapter = temperatureAdapter
        }
        lazyLoading()
    }

    //paging for recyclerview
    private fun lazyLoading() {
        binding.rvTemperature.addOnScrollListener(object : EndlessRecyclerOnScrollListener() {
            override fun onLoadMore() {
                if (isLoadMore) {
                    page++
                    viewModel.getAllTemperatureFromDB(page)
                }
            }
        })
    }

    //get last known location from fused location api, if it is null than fet from local database
    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation() {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                location?.let {
                    viewModel.getWeatherAPICall(it.latitude, it.longitude)
                } ?: run {
                    viewModel.getLatestTemperatureFromDB()
                }
            }
    }

    //check fine location permission, if available than check for GPS is on or off
    private fun checkPermission() {
        when {
            LocationPermissionUtils.isAccessFineLocationGranted(this) -> {
                checkGPSAvailable()
            }
            else -> {
                LocationPermissionUtils.requestAccessFineLocationPermission(
                    this,
                    LOCATION_PERMISSION_REQ_CODE
                )
            }
        }
    }

    //location/gps is on, if on than get last known location from fused
    private fun checkGPSAvailable() {
        if (LocationPermissionUtils.isLocationEnabled(this)) {
            getLastKnownLocation()
        } else {
            showLocationPrompt()
        }
    }

    //if GPS is not on than prompt turn on location dialog
    private fun showLocationPrompt() {
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        val result: Task<LocationSettingsResponse> =
            LocationServices.getSettingsClient(this).checkLocationSettings(builder.build())

        result.addOnCompleteListener { task ->
            try {
                val response = task.getResult(ApiException::class.java)
                getLastKnownLocation()
            } catch (exception: ApiException) {
                when (exception.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                        try {
                            val resolvable: ResolvableApiException =
                                exception as ResolvableApiException
                            resolvable.startResolutionForResult(
                                this, LocationRequest.PRIORITY_HIGH_ACCURACY
                            )
                        } catch (e: IntentSender.SendIntentException) {
                            Log.d(
                                TAG,
                                "showLocationPrompt SendIntentException exception : ${e.message}"
                            )

                        } catch (e: ClassCastException) {
                            Log.d(
                                TAG,
                                "showLocationPrompt ClassCastException exception : ${e.message}"
                            )
                        }
                    }
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_PERMISSION_REQ_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkGPSAvailable()
                } else if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    toast(getString(R.string.location_permission_not_granted))
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            LocationRequest.PRIORITY_HIGH_ACCURACY -> {
                if (resultCode == Activity.RESULT_OK) {
                    getLastKnownLocation()
                } else {
                    //user denied for turn on location, than fetch it from local database if available in local db
                    viewModel.getLatestTemperatureFromDB()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_save -> {
                checkPermission()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val LOCATION_PERMISSION_REQ_CODE = 789
    }
}