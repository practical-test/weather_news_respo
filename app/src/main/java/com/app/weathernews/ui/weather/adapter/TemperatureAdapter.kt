package com.app.weathernews.ui.weather.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.weathernews.R
import com.app.weathernews.base.extension.changeDateFormat
import com.app.weathernews.data.response.weather.Temperature
import com.app.weathernews.util.DateUtils
import kotlinx.android.synthetic.main.item_recent_temperature.view.*
import java.util.*

class TemperatureAdapter(
    val context: Context,
    var temperatureList: List<Temperature>,
) : RecyclerView.Adapter<TemperatureAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val itemLayoutView: View =
            LayoutInflater.from(context).inflate(R.layout.item_recent_temperature, parent, false)
        return MyViewHolder(itemLayoutView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.setData(position)
    }

    override fun getItemCount(): Int {
        return temperatureList.size
    }

    inner class MyViewHolder(itemLayoutView: View?) : RecyclerView.ViewHolder(itemLayoutView!!) {
        fun setData(position: Int) {
            val temperature = temperatureList[position]
            itemView.tv_temperature.text =
                StringBuilder(context.resources.getString(R.string.temperature))
                    .append(" ")
                    .append(temperature.temp.toInt())
                    .append(" ")
                    .append(context.resources.getString(R.string.celsius))

            val date = Date(temperature.created_time)
            itemView.tv_date.text =
                StringBuilder(context.resources.getString(R.string.date))
                    .append(" ")
                    .append(date.changeDateFormat(DateUtils.DATE_FORMAT_WITH_SECOND))
        }
    }
}
