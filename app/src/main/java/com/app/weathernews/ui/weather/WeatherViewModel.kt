package com.app.weathernews.ui.weather

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.weathernews.R
import com.app.weathernews.base.network.ApiResult
import com.app.weathernews.base.network.Resource
import com.app.weathernews.data.db.WeatherDatabase
import com.app.weathernews.data.db.entity.LocationData
import com.app.weathernews.data.repository.WeatherLocalRepository
import com.app.weathernews.data.repository.WeatherRepository
import com.app.weathernews.data.response.weather.Temperature
import kotlinx.coroutines.launch

class WeatherViewModel(
    private val context: Context,
    private val weatherRepository: WeatherRepository,
    private val weatherLocalRepository: WeatherLocalRepository
) : ViewModel() {
    private val _temperature = MutableLiveData<Resource<Temperature>>()
    val temperature: LiveData<Resource<Temperature>>
        get() = _temperature

    private val _lastLocalLocation = MutableLiveData<LocationData>()
    val lastLocalLocation: LiveData<LocationData>
        get() = _lastLocalLocation

    private val _temperatureAll = MutableLiveData<List<Temperature>>()
    val temperatureAll: LiveData<List<Temperature>>
        get() = _temperatureAll


    //call get weather api
    fun getWeatherAPICall(lat: Double, lon: Double) {
        viewModelScope.launch {
            _temperature.postValue(Resource.loading(null))
            when (val result = weatherRepository.getWeather(lat, lon)) {
                is ApiResult.Success -> {
                    val response = result.data
                    _temperature.postValue(Resource.success(response))
                }
                is ApiResult.Error -> {
                    //check from local database
                    weatherLocalRepository.getLatestTemperature().let {
                        _temperature.value = Resource.success(it)
                    }

                    _temperature.postValue(Resource.error(result.message.toString(), null))
                }
                is ApiResult.NoInternet -> {
                    result.data.let {
                        _temperature.value = Resource.success(it)
                    }

                    _temperature.postValue(
                        Resource.noNetwork(
                            context.resources.getString(R.string.NO_INTERNET_CONNECTION),
                            null
                        )
                    )
                }
            }
        }
    }

    //get last location from database
    fun getLatestTemperatureFromDB() {
        viewModelScope.launch {
            _lastLocalLocation.postValue(weatherLocalRepository.getLastLocationData())
        }
    }

    //get last location from database
    fun getAllTemperatureFromDB(page: Int = 1) {
        viewModelScope.launch {
             val offset = (page - 1) * WeatherDatabase.RECORDS_PER_PAGE
            _temperatureAll.postValue(weatherLocalRepository.getAllTemperature(offset))
        }
    }
}