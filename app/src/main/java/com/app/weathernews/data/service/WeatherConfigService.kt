package com.app.weathernews.data.service

import com.app.weathernews.BuildConfig
import com.app.weathernews.data.response.weather.WeatherResponse
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherConfigService {

    @GET("weather")
    suspend fun getWeather(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("units") units: String,
        @Query("appid") marketCode: String
    ): WeatherResponse

    companion object : KoinComponent {
        private val okHttpClient: OkHttpClient by inject(named("timeout")) { parametersOf(30L) }
        private val retrofitBuilder: Retrofit.Builder by inject { parametersOf(BuildConfig.API_BASE_URL) }

        fun create(): WeatherConfigService {
            val clientBuilder = okHttpClient.newBuilder()

            clientBuilder.addInterceptor { chain ->
                val newRequestBuilder = chain.request().newBuilder()
                    .addHeader("Accept", "application/")
                    .addHeader("Content-Type", "application/json")
                chain.proceed(newRequestBuilder.build())
            }

            if (BuildConfig.DEBUG) {//if debug mode than print http logs
                clientBuilder.addInterceptor(HttpLoggingInterceptor().apply {
                    setLevel(HttpLoggingInterceptor.Level.BODY)
                })
            }

            return retrofitBuilder
                .client(clientBuilder.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
                .create(WeatherConfigService::class.java)
        }
    }
}