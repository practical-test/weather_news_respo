package com.app.weathernews.data.repository

import com.app.weathernews.data.db.WeatherDatabase
import com.app.weathernews.data.db.entity.LocationData
import com.app.weathernews.data.response.weather.Temperature

class WeatherLocalRepository(private val weatherDatabase: WeatherDatabase) {

    suspend fun insertLocationData(locationData: LocationData) =
        weatherDatabase.getLocationDataDao().insertLocationData(locationData)

    suspend fun getLastLocationData() = weatherDatabase.getLocationDataDao().getLastLocationData()
    suspend fun clearLocationData() = weatherDatabase.getLocationDataDao().clearLocationData()


    suspend fun insertTemperature(temperature: Temperature) =
        weatherDatabase.getTemperatureDao().insertTemperature(temperature)

    suspend fun getLatestTemperature() = weatherDatabase.getTemperatureDao().getLatestTemperature()
    suspend fun getAllTemperature(offset: Int) =
        weatherDatabase.getTemperatureDao().getAllTemperature(offset)
}