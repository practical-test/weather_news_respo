package com.app.weathernews.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.app.weathernews.data.db.dio.LocationDataDao
import com.app.weathernews.data.db.dio.TemperatureDao
import com.app.weathernews.data.db.entity.LocationData
import com.app.weathernews.data.response.weather.Temperature

@Database(
    entities = [Temperature::class, LocationData::class],
    version = 1,
    exportSchema = false
)
abstract class WeatherDatabase : RoomDatabase() {
    abstract fun getTemperatureDao(): TemperatureDao
    abstract fun getLocationDataDao(): LocationDataDao

    companion object {
        const val DB_NAME = "weather_database.db"
        const val RECORDS_PER_PAGE = 10
    }
}
