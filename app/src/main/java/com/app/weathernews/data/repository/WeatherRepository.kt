package com.app.weathernews.data.repository

import com.app.weathernews.BuildConfig
import com.app.weathernews.base.network.ApiResult
import com.app.weathernews.data.response.weather.Temperature

interface WeatherRepository {
    suspend fun getWeather(
        lat: Double,
        lon: Double,
        units: String = "metric",
        appid: String = BuildConfig.WEATHER_API_KEY
    ): ApiResult<Temperature>
}