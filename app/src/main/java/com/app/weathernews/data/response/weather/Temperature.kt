package com.app.weathernews.data.response.weather

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "temperature")
data class Temperature(
    @PrimaryKey(autoGenerate = true) val id: Int,
    val feels_like: Double,
    val humidity: Int,
    val pressure: Int,
    val temp: Double,
    val temp_max: Double,
    val temp_min: Double,
    //@ColumnInfo(defaultValue = "CURRENT_TIMESTAMP")
    var created_time: Long
)