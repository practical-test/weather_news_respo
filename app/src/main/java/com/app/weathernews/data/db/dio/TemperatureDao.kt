package com.app.weathernews.data.db.dio

import androidx.room.*
import com.app.weathernews.data.db.WeatherDatabase
import com.app.weathernews.data.response.weather.Temperature

@Dao
interface TemperatureDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTemperature(temperature: Temperature)

    //will get latest 1 record from database
    @Query("SELECT * FROM temperature ORDER BY id DESC LIMIT 1")
    suspend fun getLatestTemperature(): Temperature

    //will return record by pagination
    @Query("SELECT * FROM temperature ORDER BY id DESC LIMIT :offset,${WeatherDatabase.RECORDS_PER_PAGE}")
    suspend fun getAllTemperature(offset: Int): List<Temperature>

    //delete all records from database
    @Query("DELETE FROM temperature")
    suspend fun clearTemperature()

    @Delete
    suspend fun deleteTemperature(temperature: Temperature)
}