package com.app.weathernews.data.db.dio

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.app.weathernews.data.db.entity.LocationData

@Dao
interface LocationDataDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertLocationData(locationData: LocationData)

    //will get latest 1 record from database
    @Query("SELECT * FROM location_data ORDER BY id DESC LIMIT 1")
    suspend fun getLastLocationData(): LocationData

    //delete all records from database
    @Query("DELETE FROM location_data")
    suspend fun clearLocationData()
}