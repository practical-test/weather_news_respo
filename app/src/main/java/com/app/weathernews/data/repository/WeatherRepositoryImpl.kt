package com.app.weathernews.data.repository

import com.app.weathernews.base.network.ApiResult
import com.app.weathernews.base.network.NetworkHelper
import com.app.weathernews.base.repository.BaseAPIRepository
import com.app.weathernews.data.db.entity.LocationData
import com.app.weathernews.data.response.weather.Temperature
import com.app.weathernews.data.service.WeatherConfigService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.System.currentTimeMillis

class WeatherRepositoryImpl(
    private var service: WeatherConfigService,
    private val weatherLocalRepository: WeatherLocalRepository,
    private var networkHelper: NetworkHelper
) : BaseAPIRepository(), WeatherRepository {

    override suspend fun getWeather(
        lat: Double,
        lon: Double,
        units: String,
        appid: String
    ): ApiResult<Temperature> {
        //check internet connection
        if (networkHelper.isNetworkConnected()) {
            val temperature = ioCall({ service.getWeather(lat, lon, units, appid).temperature })
            when (temperature) {
                is ApiResult.Success -> {
                    withContext(Dispatchers.IO) {
                        val currentTimeStamp = currentTimeMillis()
                        weatherLocalRepository.clearLocationData()//delete location data from database table
                        weatherLocalRepository.insertLocationData(
                            LocationData(
                                1,
                                lat,
                                lon,
                                currentTimeStamp
                            )
                        )//insert new location data to database table

                        temperature.data.created_time = currentTimeStamp//save current time stamp
                        //weatherLocalRepository.clearTemperature()
                        weatherLocalRepository.insertTemperature(temperature.data)//save temperature
                    }
                    return temperature
                }
            }
            return temperature

        } else {//internet is not available, than get data from local storage
            val temperatureData = getLatestTemperatureFromDB()
            return ApiResult.NoInternet(temperatureData)
        }
    }

    //get Temperature data from local database
    private suspend fun getLatestTemperatureFromDB(): Temperature {
        return withContext(Dispatchers.IO) {
            weatherLocalRepository.getLatestTemperature()
        }
    }
}