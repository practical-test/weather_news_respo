package com.app.weathernews

import android.app.Application
import com.app.weathernews.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class WeatherNewsApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@WeatherNewsApp)
            modules(
                listOf(
                    coreNetworkModule,
                    databaseModule,
                    repositoryModule,
                    viewModelModule,
                    coreModule,
                )
            )
        }
    }
}